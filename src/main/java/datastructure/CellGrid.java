package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState initialState; // gird[dead][alive]
    CellState[][] rowsAndColumns;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.columns = columns;
		this.initialState = initialState;
		this.rowsAndColumns = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                rowsAndColumns[i][j] = initialState; // setter hver row og column til hva som er i initialstate
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) { // hente row and colum så gi det element verdi. så row og colum i posisjon 0,2 er død men endre til levende
        rowsAndColumns[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) { // finne cellstate på row and column henter content
        return rowsAndColumns[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(rows, columns, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) { // lage to for loops hvor i, rows og j, columns.
                newGrid.set(i, j, this.get(i, j));
            }
        }
        return newGrid;
    }
}
