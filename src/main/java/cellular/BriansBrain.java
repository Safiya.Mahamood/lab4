package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else{
                    currentGeneration.set(row, col, CellState.DEAD);
                    //currentGeneration.set(row, col, CellState.DYING);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int i = 0; i < currentGeneration.numRows(); i++) {
            for (int j = 0; j < currentGeneration.numColumns(); j++) {
                CellState nextState = getNextCell(i, j);
                nextGeneration.set(i, j, nextState);
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = currentGeneration.get(row, col);
        int amountNeighbours = countNeighbors(row, col, CellState.ALIVE);
        if(state == CellState.ALIVE){
            return CellState.DYING;
        }
        if(state == CellState.DYING){
            return CellState.DEAD;
        }
        if(state == CellState.DEAD && amountNeighbours == 2){
            return CellState.ALIVE;
        }
        return state;
    }

    private int countNeighbors(int row, int col, CellState alive) {
        int count = 0;
        for (int i = row-1; i <= row+1; i++) {
            for (int j = col-1; j <= col+1; j++) {
                if(!(i == row && j == col)
                        && (i >= 0 && i < currentGeneration.numRows())
                        && (j >= 0 && j < currentGeneration.numColumns())
                        && currentGeneration.get(i, j) == alive){
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
